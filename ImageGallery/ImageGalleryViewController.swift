//
//  ViewController.swift
//  ImageGallery
//
//  Created by zweqi on 15/06/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

class ImageGalleryViewController: UIViewController, UICollectionViewDelegate {
        
    // MARK: - Model
    
    weak var documentTVC: DocumentsTableViewController?
    var currentDocument: String? { didSet { title = currentDocument ?? nil } }

    // Data Source for ImageCollectionView
    var imageDataStorage: [ImageProperties] = [] {
        didSet{
            if currentDocument != nil {
                documentTVC?.imageGalleryData[currentDocument!] = imageDataStorage
            }
        }
    }
    
    // MARK: - Storyboard
    
    @IBOutlet weak var imageCollectionView: UICollectionView! {
        didSet {
            imageCollectionView.dataSource = self
            imageCollectionView.delegate = self
            imageCollectionView.dragDelegate = self
            imageCollectionView.dropDelegate  = self
            addPinchGesture()
        }
    }
    
    @IBOutlet weak var trashButton: UIBarButtonItem!
    
    // MARK: - Private Properties
    
    private var cellScale: CGFloat = 1.0
    private var indexPathsForDragging: [IndexPath] = []
    
    // MARK: - View Controller Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // adds a back button on the navigation panel, that changes display mode of the splitVC to the previous value
        // (in our case: from .primaryHidden to .primaryOverlay)
        navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
        // without this, back button won't show up in portrait mode
        navigationItem.leftItemsSupplementBackButton = true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if let navcon = splitViewController?.viewControllers.last as? UINavigationController {
            navcon.navigationBar.addInteraction(UIDropInteraction(delegate: self))
        } else {
            navigationController?.navigationBar.addInteraction(UIDropInteraction(delegate: self))
        }
    }
}

// MARK: - UICollectionViewDataSource

extension ImageGalleryViewController: UICollectionViewDataSource {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageDataStorage.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath)
        if let cell = cell as? ImageGalleryCollectionViewCell {
            // TODO: move updateImageForCell() to the ImageGalleryCollectionViewCell
            updateImageForCell(cell, fromURL: imageDataStorage[indexPath.item].url)
            addTapGesture(to: cell)
            flowLayout?.invalidateLayout()  // re-layout cells, adapting to their new height
        }
        return cell
    }
    
    /// Fetching an image from the given URL and setting it as a 'cell.backgroundImage' value.
    ///
    /// If the image couldn't be fetched, nil value would be set for a 'cell.backgroundImage'.
    ///
    /// - Parameters:
    ///   - cell: The cell for which the 'backgroundImage' will be updated.
    ///   - url: The URL of the image.
    private func updateImageForCell(_ cell: ImageGalleryCollectionViewCell, fromURL url: URL) {
        cell.backgroundImage = nil
        cell.spinner.startAnimating()
        
        DispatchQueue.global(qos: .userInitiated).async {
            let urlContents = try? Data(contentsOf: url)
            
            DispatchQueue.main.async { [weak self] in
                if let imageData = urlContents {
                    cell.backgroundImage = UIImage(data: imageData)
                    cell.label.isHidden = true
                } else {
                    // failed to load an image
                    let text = NSAttributedString(string: "No Image", attributes: [.font: self!.font])
                    cell.label.attributedText = text
                    cell.label.isHidden = false
                }
                cell.spinner.stopAnimating()
            }
        }
    }
    
    private var font: UIFont {
        return UIFontMetrics(forTextStyle: .body).scaledFont(for: UIFont.preferredFont(forTextStyle: .body).withSize(29.0))
    }
}

// MARK: - UICollectionViewDelegateFlowLayout

extension ImageGalleryViewController: UICollectionViewDelegateFlowLayout {

    var flowLayout: UICollectionViewFlowLayout? {
        return imageCollectionView?.collectionViewLayout as? UICollectionViewFlowLayout
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let imageAspectRatio = imageDataStorage[indexPath.item].aspectRatio
        let cellWidth = ImageGalleryCollectionViewCell.defaultWidth * cellScale
        let cellHeight = cellWidth * imageAspectRatio
        return CGSize(width: cellWidth, height: cellHeight)
    }
}

// MARK: - UICollectionViewDragDelegate

extension ImageGalleryViewController: UICollectionViewDragDelegate {

    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        session.localContext = collectionView
        indexPathsForDragging = []
        return dragItems(at: indexPath)
    }

    func collectionView(_ collectionView: UICollectionView, itemsForAddingTo session: UIDragSession, at indexPath: IndexPath, point: CGPoint) -> [UIDragItem] {
        return dragItems(at: indexPath)
    }
    
    /// Returns a drag item, holding URL of the 'backgroundImage' of the cell at the given index path in the image collection view.
    ///
    /// Drag item also stores image URL in its 'localObject' property.
    ///
    /// - Parameters:
    ///   - indexPath: The index path of the cell being dragged.
    private func dragItems(at indexPath: IndexPath) -> [UIDragItem] {
        guard imageCollectionView.cellForItem(at: indexPath) is ImageGalleryCollectionViewCell else { return [] }
        
        indexPathsForDragging += [indexPath]
        let imageURL = imageDataStorage[indexPath.item].url as NSURL
        let dragItem = UIDragItem(itemProvider: NSItemProvider(object: imageURL))
        dragItem.localObject = imageURL
        return [dragItem]
    }
}

// MARK: - UICollectionViewDropDelegate

extension ImageGalleryViewController: UICollectionViewDropDelegate {

    func collectionView(_ collectionView: UICollectionView, canHandle session: UIDropSession) -> Bool {
        // don't accept drops if no documents were selected
        guard currentDocument != nil else { return false }
        
        let isSelf = (session.localDragSession?.localContext as? UICollectionView) == collectionView
        if isSelf {
            // for local: accept drops of items that have NSURL representation
            return session.canLoadObjects(ofClass: NSURL.self)
        } else {
            // for non-local: accept drops of items that have both NSURL and UIImage representations
            return session.canLoadObjects(ofClass: NSURL.self) && session.canLoadObjects(ofClass: UIImage.self)
        }
    }

    func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal {

        // copy dragItems that go outside the app and move those that stay inside
        let isSelf = (session.localDragSession?.localContext as? UICollectionView) == collectionView
        return UICollectionViewDropProposal(operation: isSelf ? .move : .copy, intent: .insertAtDestinationIndexPath)
    }

    // performing a drop operation
    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
        let section = collectionView.numberOfSections - 1
        let item = collectionView.numberOfItems(inSection: section)
        let destinationIndexPath = coordinator.destinationIndexPath ?? IndexPath(item: item, section: section)

        for item in coordinator.items {
            if let sourceIndexPath = item.sourceIndexPath {
                // local drag and drop
                moveItem(item, from: sourceIndexPath, to: destinationIndexPath, withCoordinator: coordinator, inCollectionView: collectionView)
            } else {
                // non-local drag and drop
                insertItem(item, at: destinationIndexPath, withCoordinator: coordinator, inCollectionView: collectionView)
            }
        }
    }
    
    /// This method moves a cell from 'sourceIndexPath' to 'destinationIndexPath' within the same collection view.
    ///
    /// - Parameters:
    ///   - item: The item being dragged.
    ///   - sourceIndexPath: The index path of the item in the collection view.
    ///   - destinationIndexPath: The index path in the collection view at which the content would be dropped.
    ///   - coordinator: The coordinator object, obtained from performDropWith: UICollectionViewDropDelegate method.
    ///   - collectionView: The collection view in which moving needs to be done.
    private func moveItem(_ item: UICollectionViewDropItem, from sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath, withCoordinator coordinator: UICollectionViewDropCoordinator, inCollectionView collectionView: UICollectionView) {
        
        if let imageURL = item.dragItem.localObject as? URL {
            let imageAspectRatio = imageDataStorage[sourceIndexPath.item].aspectRatio
            let imageData = ImageProperties(url: imageURL, aspectRatio: imageAspectRatio)
            
            collectionView.performBatchUpdates({
                imageDataStorage.remove(at: sourceIndexPath.item)
                imageDataStorage.insert(imageData, at: destinationIndexPath.item)
                collectionView.deleteItems(at: [sourceIndexPath])
                collectionView.insertItems(at: [destinationIndexPath])
            })
            coordinator.drop(item.dragItem, toItemAt: destinationIndexPath)
        }
    }

    /// This method inserts a new cell at the 'destinationIndexPath' in the given collection view, based on an 'item' being dragged from another app.
    ///
    /// - Parameters:
    ///   - item: The item being dragged.
    ///   - destinationIndexPath: The index path in the collection view at which the content would be dropped.
    ///   - coordinator: The coordinator object, obtained from performDropWith: UICollectionViewDropDelegate method.
    ///   - collectionView: The collection view in which inserting needs to be done.
    private func insertItem(_ item: UICollectionViewDropItem, at destinationIndexPath: IndexPath, withCoordinator coordinator: UICollectionViewDropCoordinator, inCollectionView collectionView: UICollectionView) {
        
        // instantly putting a placeholder, because the drop may take some time
        let placeholderContext = coordinator.drop(
            item.dragItem,
            to: UICollectionViewDropPlaceholder(insertionIndexPath: destinationIndexPath, reuseIdentifier: "DropPlaceholderCell")
        )
        
        // creating a dispatch group to start tasks concurrently and then get notified when they're finished
        let imagePropertiesGroup = DispatchGroup()
        
        // getting image URL from the dropping item
        var dropURL: URL?
        
        imagePropertiesGroup.enter()
        item.dragItem.itemProvider.loadObject(ofClass: NSURL.self) { (provider, error) in
            if let url = provider as? URL {
                dropURL = url.imageURL
            } else {
                print("ERROR: Failed to load URL: \(error?.localizedDescription ?? "Uknown reason.")" )
                placeholderContext.deletePlaceholder()
            }
            imagePropertiesGroup.leave()
        }
        
        // getting image from the dropping item
        var dropImage: UIImage?
        
        imagePropertiesGroup.enter()
        item.dragItem.itemProvider.loadObject(ofClass: UIImage.self) { (provider, error) in
            if let image = provider as? UIImage {
                dropImage = image
            } else {
                print("ERROR: Failed to load Image: \(error?.localizedDescription ?? "Uknown reason.")" )
                placeholderContext.deletePlaceholder()
            }
            imagePropertiesGroup.leave()
        }

        // updating data source with image and image URL
        imagePropertiesGroup.notify(queue: DispatchQueue.main) { [weak self] in
            let dropAspectRatio = dropImage!.size.height / dropImage!.size.width
            let imageData = ImageProperties(url: dropURL!, aspectRatio: dropAspectRatio)
            placeholderContext.commitInsertion(dataSourceUpdates: { insertionIndexPath in
                self?.imageDataStorage.insert(imageData, at: insertionIndexPath.item)
            })
        }
    }
}

// MARK: - UIDropInteractionDelegate

extension ImageGalleryViewController: UIDropInteractionDelegate {
 
    func dropInteraction(_ interaction: UIDropInteraction, canHandle session: UIDropSession) -> Bool {
        return session.canLoadObjects(ofClass: NSURL.self)
    }
    
    func dropInteraction(_ interaction: UIDropInteraction, sessionDidUpdate session: UIDropSession) -> UIDropProposal {
        guard let trashButtonView = trashButton.value(forKey: "view") as? UIView else { return UIDropProposal(operation: .cancel) }
        
        let currentDropPoint = session.location(in: trashButtonView)
        let isDraggingOverTrashButton = trashButtonView.bounds.contains(currentDropPoint)
        return UIDropProposal(operation: isDraggingOverTrashButton ? .move : .cancel)
    }
    
    func dropInteraction(_ interaction: UIDropInteraction, performDrop session: UIDropSession) {
        indexPathsForDragging.sorted(by: >).forEach { indexPath in
            imageDataStorage.remove(at: indexPath.item)
        }
        imageCollectionView.deleteItems(at: indexPathsForDragging)
    }
}

// MARK: - Gestures

extension ImageGalleryViewController {
    
    /// Adds pinch gesture to the current image collection view.
    ///
    /// Pinch allows the user to change scale of width of the cells in this collection view.
    private func addPinchGesture() {
        let pinch = UIPinchGestureRecognizer(
            target: self,
            action: #selector(pinch(recognizer:))
        )
        imageCollectionView.addGestureRecognizer(pinch)
    }
    
    /// Changes scale of width of cells in image collection view.
    ///
    /// - Parameters:
    ///   - recognizer: The gesture recognizer provided by pinch gesture.
    @objc private func pinch(recognizer: UIPinchGestureRecognizer) {
        switch recognizer.state {
        case .changed:
            cellScale = recognizer.scale
            flowLayout?.invalidateLayout()
        case .ended:
            //saving new size of the cell
            if cellScale != 1.0 {
                ImageGalleryCollectionViewCell.defaultWidth *= cellScale
                cellScale = 1.0
                imageCollectionView.reloadData()
            }
        default:
            return
        }
    }
    
    /// Adds a single tap gesture to the image gallery collection view cell.
    ///
    /// Tap on cell opens a new MVC, which presents the background image of this cell
    /// - Parameters:
    ///   - cell: The cell to which tap gesture is being added.
    private func addTapGesture(to cell: ImageGalleryCollectionViewCell) {
        let tapGesture = UITapGestureRecognizer(
            target: self,
            action: #selector(tap(recognizer:))
        )
        cell.addGestureRecognizer(tapGesture)
    }
    
    /// Opens a new MVC, which presents the background image of this cell
    ///
    /// - Parameters:
    ///   - recognizer: The gesture recognizer provided by tap gesture.
    @objc private func tap(recognizer: UITapGestureRecognizer) {
        if let indexPath = imageCollectionView.indexPathForItem(at: recognizer.location(in: imageCollectionView)) {
            let cell = imageCollectionView.cellForItem(at: indexPath) as! ImageGalleryCollectionViewCell
            
            guard cell.backgroundImage != nil else { return }
            performSegue(withIdentifier: "ShowImage", sender: cell)
        }
    }
}

// MARK: - Navigation

extension ImageGalleryViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowImage" {
            if let imageVC = segue.destination as? ImageViewController,
                let imageCell = sender as? ImageGalleryCollectionViewCell {
                imageVC.backgroundImage = imageCell.backgroundImage
            }
        }
    }
}
