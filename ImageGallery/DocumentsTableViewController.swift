//
//  DocumentsTableViewController.swift
//  ImageGallery
//
//  Created by zweqi on 22/06/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

typealias ImageGallery = [String: [ImageProperties]]

struct ImageProperties: Codable {
    let url: URL
    let aspectRatio: CGFloat    // image height to width ratio
}

class DocumentsTableViewController: UITableViewController {
    
    // MARK: - Model
    
    var imageGalleryData: ImageGallery {
        get {
            if let data = UserDefaults.standard.object(forKey: "ImageGallery") as? Data {
                let decoder = PropertyListDecoder()
                let imageGallery = try? decoder.decode(ImageGallery.self, from: data)
                return imageGallery ?? [:]
            } else {
                return [:]
            }
        }
        set {
            let encoder = PropertyListEncoder()
            let encodedData = try? encoder.encode(newValue)
            UserDefaults.standard.set(encodedData, forKey: "ImageGallery")
        }
    }
    
    var documents: [String] = []
    var recentlyDeletedDocuments: [String] = []

    // MARK: - Storyboard
    
    @IBAction func newDocument(_ sender: UIBarButtonItem) {
        let newDocument = "Untitled".madeUnique(withRespectTo: Array(imageGalleryData.keys))
        documents += [newDocument]
        imageGalleryData[newDocument] = []
        tableView.reloadData()
    }

    // MARK: - View Controller Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        splitViewController?.delegate = self
        documents = Array(imageGalleryData.keys)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        // In primaryOverlay mode, master VC partially obscures detail VC
        // and we can slide in/out master VC (even in landscape mode).
        // Put here because when the layout changes (of the Split View) it often resets that preferred mode.
        // Using "if" statement, otherwise there will be an infinite loop of Layout calls.
        if (UIDevice.current.orientation == .landscapeLeft || UIDevice.current.orientation == .landscapeRight), splitViewController?.preferredDisplayMode != .primaryOverlay {
            splitViewController?.preferredDisplayMode = .primaryOverlay
        }
    }
}

// MARK: - UITableViewDelegate

extension DocumentsTableViewController {
    
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        let cell = tableView.cellForRow(at: indexPath) as! DocumentsTableViewCell

        if cell.reuseIdentifier == "RecentlyDeletedCell" {
            return nil
        } else {
            return indexPath
        }
    }
    
    override func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let cell = tableView.cellForRow(at: indexPath) as! DocumentsTableViewCell
        
        switch cell.reuseIdentifier {
        case "RecentlyDeletedCell":
            // move a document from Recently Deleted to Documents
            let swipeAction = UIContextualAction(style: .normal, title: "Recover") {
                (action, sourceView, actionPerformed) in
                
                let deletedDocument = cell.textField.text!
                let newIndexPath = IndexPath(row: self.documents.count, section: 0)
                
                tableView.performBatchUpdates({
                    self.recentlyDeletedDocuments.remove(at: indexPath.row)
                    self.documents += [deletedDocument]
                    // not moveRow(), because we want a different prototype cell to be created
                    tableView.deleteRows(at: [indexPath], with: .fade)
                    tableView.insertRows(at: [newIndexPath], with: .fade)
                    
                    // reloading data, because there is a bug with "Undelete" button, that causes it to stay on screen even after the swipe action, until you select another document
                    // (may be it is only on an iPad simulator)
                    // otherwise we'd use reloadSections() method for a section header and rows
                    tableView.reloadData()
                })
            }
            swipeAction.backgroundColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
            
            let swipeConfiguration = UISwipeActionsConfiguration(actions: [swipeAction])
            swipeConfiguration.performsFirstActionWithFullSwipe = true
            return swipeConfiguration
        default:
            return nil
        }
    }
}

// MARK: - UITableViewDataSource

extension DocumentsTableViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0: return documents.count
        case 1: return recentlyDeletedDocuments.count
        default: return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RecentlyDeletedCell", for: indexPath) as! DocumentsTableViewCell
            cell.textField.text = recentlyDeletedDocuments[indexPath.row]
            cell.parentViewController = self
            cell.addTapGestures()
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DocumentCell", for: indexPath) as! DocumentsTableViewCell
            cell.textField.text = documents[indexPath.row]
            cell.parentViewController = self
            cell.addTapGestures()
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0, documents.count > 0 {
            return "Documents"
        } else if section == 1, recentlyDeletedDocuments.count > 0 {
            return "Recently Deleted"
        } else {
            return nil
        }
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! DocumentsTableViewCell

        if editingStyle == .delete {
            switch cell.reuseIdentifier {
            case "DocumentCell":
                // move a document from Documents to Recently Deleted
                let deletedDocument = cell.textField.text!
                let newIndexPath = IndexPath(row: recentlyDeletedDocuments.count, section: 1)
                
                tableView.performBatchUpdates({
                    documents.remove(at: indexPath.row)
                    recentlyDeletedDocuments += [deletedDocument]
                    // not moveRow(), because we want a different prototype cell to be created
                    tableView.deleteRows(at: [indexPath], with: .fade)
                    tableView.insertRows(at: [newIndexPath], with: .fade)
                })
            case "RecentlyDeletedCell":
                // permanently delete a document
                let deletedDocument = cell.textField.text!
                recentlyDeletedDocuments.remove(at: indexPath.row)
                imageGalleryData[deletedDocument] = nil
                tableView.deleteRows(at: [indexPath], with: .fade)
            default:
                break
            }
        }
    }
}

// MARK: - Navigation

extension DocumentsTableViewController {
    
    // not using shouldPerformSegue(), because we hooked up the segue only to the needed cells (with prototype "DocumentCell")
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowImageCollection" {
            if let navcon = segue.destination as? UINavigationController,
                let imageGalleryVC = navcon.visibleViewController as? ImageGalleryViewController,
                let document = (sender as? DocumentsTableViewCell)?.textField.text! {
                
                imageGalleryVC.documentTVC = self
                imageGalleryVC.currentDocument = document
                imageGalleryVC.imageDataStorage = imageGalleryData[document]!
                splitViewController?.toggleMasterView()
            }
        }
    }
}

// MARK: - UISplitViewControllerDelegate

extension DocumentsTableViewController: UISplitViewControllerDelegate {
    
    func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController: UIViewController, onto primaryViewController: UIViewController) -> Bool {
        
        // we don't want to collapse when secondary VC (image collection view) doesn't have a current document (i.e. first load in portrait mode)
        if let navcon = secondaryViewController as? UINavigationController,
            let imageGalleryVC = navcon.visibleViewController as? ImageGalleryViewController,
            imageGalleryVC.currentDocument == nil {
            return true
        }
        return false
    }
}


