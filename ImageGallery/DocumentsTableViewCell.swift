//
//  DocumentsTableViewCell.swift
//  ImageGallery
//
//  Created by zweqi on 25/06/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

class DocumentsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var textField: UITextField! { didSet { textField.delegate = self } }
    private var previousTitle: String?  // previous value of the textField.text
    
    weak var parentViewController: DocumentsTableViewController!
}

// MARK: - Gestures

extension DocumentsTableViewCell {
    
    func addTapGestures() {
        // one tap to open up Image Collection
        let singleTapGesture = UITapGestureRecognizer(
            target: self,
            action: #selector(singleTap(recognizer:))
        )
        singleTapGesture.numberOfTapsRequired = 1
        addGestureRecognizer(singleTapGesture)
        
        // two taps for editing cell's textfield
        let doubleTapGesture = UITapGestureRecognizer(
            target: self,
            action: #selector(doubleTap(recognizer:))
        )
        doubleTapGesture.numberOfTapsRequired = 2
        addGestureRecognizer(doubleTapGesture)
        
        singleTapGesture.require(toFail: doubleTapGesture)
    }
    
    @objc func singleTap(recognizer: UITapGestureRecognizer) {
        disableAnyEnabledTextField()
        
        let indexPath = parentViewController.tableView.indexPath(for: self)
        parentViewController.tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
        
        if reuseIdentifier == "DocumentCell" {
            parentViewController.performSegue(withIdentifier: "ShowImageCollection", sender: self)
        }
    }
    
    // disables any enabled text fields of the cells in current table view
    private func disableAnyEnabledTextField() {
        parentViewController.tableView.visibleCells.forEach { cell in
            if let cell = cell as? DocumentsTableViewCell {
                cell.textField.isEnabled = false
                cell.textField.resignFirstResponder()
            }
        }
    }
    
    @objc func doubleTap(recognizer: UITapGestureRecognizer) {
        textField.isEnabled = true
        textField.becomeFirstResponder()
    }
}

// MARK: - UITextFieldDelegate

extension DocumentsTableViewCell: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        previousTitle = textField.text
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard textField.text != previousTitle else { return }
        updateTextField()
        updateTableView()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // updates text field's text with a unique value with respect to other documents
    private func updateTextField() {
        let allDocuments = Array(parentViewController.imageGalleryData.keys)
        textField.text = textField.text!.madeUnique(withRespectTo: allDocuments)
        textField.isEnabled = false
    }
    
    // updates title of changed document in table view datasource
    private func updateTableView() {
        let newTitle = textField.text!
        let row = parentViewController.tableView.indexPath(for: self)!.row
        let imageCollection = parentViewController.imageGalleryData[previousTitle!]
        
        parentViewController.tableView.performBatchUpdates({
            if reuseIdentifier == "RecentlyDeletedCell" {
                parentViewController.recentlyDeletedDocuments.remove(at: row)
                parentViewController.recentlyDeletedDocuments.insert(newTitle, at: row)
            } else {
                parentViewController.documents.remove(at: row)
                parentViewController.documents.insert(newTitle, at: row)
            }
            parentViewController.imageGalleryData.removeValue(forKey: previousTitle!)
            parentViewController.imageGalleryData[newTitle] = imageCollection
        })
    }
}

