//
//  ImageGalleryCollectionViewCell.swift
//  ImageGallery
//
//  Created by zweqi on 15/06/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

class ImageGalleryCollectionViewCell: UICollectionViewCell {
    
    // label will be shown when there is no background image available for this cell
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    var backgroundImage: UIImage? { didSet { setNeedsDisplay() } }
    
    override func draw(_ rect: CGRect) {
        backgroundImage?.draw(in: bounds)
    }
}

extension ImageGalleryCollectionViewCell {
    static var defaultWidth: CGFloat = 139
}

